<?php
/**
 * Grundeinstellungen für WordPress
 *
 * Zu diesen Einstellungen gehören:
 *
 * * MySQL-Zugangsdaten,
 * * Tabellenpräfix,
 * * Sicherheitsschlüssel
 * * und ABSPATH.
 *
 * Mehr Informationen zur wp-config.php gibt es auf der
 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Zugangsdaten für die MySQL-Datenbank
 * bekommst du von deinem Webhoster.
 *
 * Diese Datei wird zur Erstellung der wp-config.php verwendet.
 * Du musst aber dafür nicht das Installationsskript verwenden.
 * Stattdessen kannst du auch diese Datei als wp-config.php mit
 * deinen Zugangsdaten für die Datenbank abspeichern.
 *
 * @package WordPress
 */

// ** MySQL-Einstellungen ** //
/**   Diese Zugangsdaten bekommst du von deinem Webhoster. **/


// define('DB_HOST', 'localhost');
// define('DB_NAME', 'heye_universal_heldenchor');
// define('DB_USER', 'root');
// define('DB_PASSWORD', '');


// define('DB_HOST', 'localhost');
// define('DB_NAME', 'heldenchor');
// define('DB_USER', 'heldenchor');
// define('DB_PASSWORD', 'Ndun38fnndf');


define('DB_HOST', 'localhost');
define('DB_NAME', 'heye-universal');
define('DB_USER', 'heye-universal');
define('DB_PASSWORD', 'Z09Y8Jne2Nvahl');


/**
 * Der Datenbankzeichensatz, der beim Erstellen der
 * Datenbanktabellen verwendet werden soll
 */
define('DB_CHARSET', 'utf8mb4');

/**
 * Der Collate-Type sollte nicht geändert werden.
 */
define('DB_COLLATE', '');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden untenstehenden Platzhaltertext in eine beliebige,
 * möglichst einmalig genutzte Zeichenkette.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle Schlüssel generieren lassen.
 * Du kannst die Schlüssel jederzeit wieder ändern, alle angemeldeten
 * Benutzer müssen sich danach erneut anmelden.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1+^b:zlnuJSzkYg,^_y|-Uq?`ll,%Y`+6:O-2Y5:qkq K!Aiujzq=rjlPA4L<|R&');
define('SECURE_AUTH_KEY',  'w(j2!Kvf ;3Iq[bf=_5NN%:9#u-d3({-n!}k+YSdo25N!bHeob{M|%tmb+F)|3VO');
define('LOGGED_IN_KEY',    '}8n@TmDpPP;hxR`qnA,)ld1`.f84ZNi[ybp1|CqZ;4- <z?Hq-N%;IBJ/1tHP2g/');
define('NONCE_KEY',        'nw:&|=wM 5%9`tP%5#sCGpM68bI+-U`oib(]W?pH}.3~,IMBr>&rJibm4^|-zn}_');
define('AUTH_SALT',        '++-bPT5a|pV87ew9$YzaGev/(GxiWrg(.Jt 23I/:?L3D9B[g1PCXP:>G{ey)XhJ');
define('SECURE_AUTH_SALT', 'yo`8=Mc@7?q<[X_[CGSANC+t+u;@S,zO}em%ITwCd-1B:/@ChVobI&R{Dj>`-MG-');
define('LOGGED_IN_SALT',   '-Nu(/.!@&Ok>7F$F]{B`4L)u*#1=*B4 v,+AdSj/~QPLd>c%P[Q;lW04!dq:Rt$M');
define('NONCE_SALT',       'D}6fYJ34-]7$9EO]kqrN *%k^nJ,Qk,V>L,xPO/5S`^<P:VswH)t-|+P8U` 3cLN');

/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 * Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 * verschiedene WordPress-Installationen betreiben.
 * Bitte verwende nur Zahlen, Buchstaben und Unterstriche!
 */
$table_prefix  = 'wp_';

/**
 * Für Entwickler: Der WordPress-Debug-Modus.
 *
 * Setze den Wert auf „true“, um bei der Entwicklung Warnungen und Fehler-Meldungen angezeigt zu bekommen.
 * Plugin- und Theme-Entwicklern wird nachdrücklich empfohlen, WP_DEBUG
 * in ihrer Entwicklungsumgebung zu verwenden.
 *
 * Besuche den Codex, um mehr Informationen über andere Konstanten zu finden,
 * die zum Debuggen genutzt werden können.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
 // define('WP_DEBUG', true);
 define( 'WP_DEBUG_LOG', true);

/* Das war’s, Schluss mit dem Bearbeiten! Viel Spaß beim Bloggen. */
/* That's all, stop editing! Happy blogging. */

/** Der absolute Pfad zum WordPress-Verzeichnis. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Definiert WordPress-Variablen und fügt Dateien ein.  */
require_once(ABSPATH . 'wp-settings.php');
