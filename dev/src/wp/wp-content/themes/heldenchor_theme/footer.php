

	<?php  //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: JS - WebApp ?>


	<?php
		$siteTitle = get_bloginfo ();
		$siteRoot = get_site_url ();
		$siteDomain = "http://$_SERVER[HTTP_HOST]";
		$siteTemplateUri = get_template_directory_uri ();
		$versionLoad = "1.0.1.45";
		$wordpressVersion = get_bloginfo ('version');
	?>


	<script>
		var siteTitle = "<?php echo $siteTitle; ?>";
		var siteRoot = "<?php echo $siteRoot; ?>";
		var siteDomain = "http://" + siteRoot.replace ("http://", "").split ("/")[0];

		var routerRootPath = siteRoot;
		var siteRootSplit = siteRoot.split (siteDomain);
		if  (siteRootSplit.length === 2) routerRootPath = siteRootSplit[1];

		var webAppConfig =
		{
			siteTitle: siteTitle,
			siteDomain: siteDomain,
			siteRoot: siteRoot,
			siteTemplateUri: "<?php echo $siteTemplateUri; ?>",
			routerRootPath: routerRootPath,
			versionLoad: "<?php echo $versionLoad; ?>",
			wordpressVersion: "<?php echo $wordpressVersion; ?>"
		}

		console.info (webAppConfig);
	</script>



	<?php
		if ($siteDomain === "http://localhost:8000" || $siteDomain === "http://10.211.55.2:8000")
		{
			$jsUri = $siteTemplateUri."/webApp/js/";
		}
		else
		{
			$jsUri = $siteTemplateUri."/webApp/js_bin/";
		}

		$webApp = $jsUri.'webApp.js?'.$versionLoad;
	?>
	<!--
	-->
	<script data-main="<?php echo $webApp ?>" src="<?php echo $jsUri, 'lib/require/require.min.js'; ?>"></script>


	<?php  //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: JS - Google Analytics ?>


	<script>
		// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		// (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		// m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		// })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		// ga('create', 'UA-83919849-1', 'auto');
		// ga('send', 'pageview');
	</script>


	<?php  //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::  ?>

</body>
</html>
