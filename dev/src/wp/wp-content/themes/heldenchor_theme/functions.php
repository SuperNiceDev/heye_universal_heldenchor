<?php

/**
 *
 * functions and definitions
 *
 */



//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: REST API


include (get_template_directory() . '/restservice.php');


//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: Alphabetical Post List


function custom_post_order ($query)
{
    /*
        Set post types.
        _builtin => true returns WordPress default post types.
        _builtin => false returns custom registered post types.
    */
    $post_types = get_post_types(array('_builtin' => true), 'names');
    /* The current post type. */
    $post_type = $query->get('post_type');
    /* Check post types. */
    if(in_array($post_type, $post_types)){
        /* Post Column: e.g. title */
        if($query->get('orderby') == ''){
            $query->set('orderby', 'title');
        }
        /* Post Order: ASC / DESC */
        if($query->get('order') == ''){
            $query->set('order', 'ASC');
        }
    }

    // $query->set( 'posts_per_page', '-1' );
}

// if (is_admin())
// {
    add_action ('pre_get_posts', 'custom_post_order');
// }


//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: Hide Permalink


add_action ('admin_head', 'wpds_custom_admin_post_css');

function wpds_custom_admin_post_css ()
{
	$post = get_post ();
	if ($post->post_type == 'page')
	{
		// echo "<style>#edit-slug-box {display:none;}</style>";
	}
}


//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: Hide


add_action ('admin_head', 'my_custom_css');

function my_custom_css ()
{
	echo '<style>
		.column-featured_image
		{
			display: none;
		}

		#wp-admin-bar-wp-logo
		{
			display: none;
		}

		.wp-editor-tools
		{
			display: none;
		}

		.mce-toolbar .mce-btn i.mce-i-rml-folder-gallery
		{
			display: none;
		}

		.wp-media-buttons .insert-media
		{
			display: none;
		}
	</style>';
}


//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: CHECK MOBILE


function isMobileDevice ()
{
	$aMobileUA = array(
		'/iphone/i' => 'iPhone',
		'/ipod/i' => 'iPod',
		'/ipad/i' => 'iPad',
		'/android/i' => 'Android',
		'/blackberry/i' => 'BlackBerry',
		'/webos/i' => 'Mobile'
	);

	//Return true if Mobile User Agent is detected
	foreach($aMobileUA as $sMobileKey => $sMobileOS)
	{
		if(preg_match($sMobileKey, $_SERVER['HTTP_USER_AGENT']))
		{
			return true;
		}
	}
	//Otherwise return false..
	return false;
}


function isSmartPhoneDevice ()
{
	$aMobileUA = array(
		'/iphone/i' => 'iPhone',
		'/ipod/i' => 'iPod',
		'/android/i' => 'Android',
		'/blackberry/i' => 'BlackBerry',
		'/webos/i' => 'Mobile'
	);

	//Return true if Mobile User Agent is detected
	foreach($aMobileUA as $sMobileKey => $sMobileOS)
	{
		if(preg_match($sMobileKey, $_SERVER['HTTP_USER_AGENT']))
		{
			return true;
		}
	}
	//Otherwise return false..
	return false;
}


//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: Remove Page Editor


function remove_pages_editor ()
{
	remove_post_type_support ('page', 'editor');
}

add_action ('init', 'remove_pages_editor');


//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: Remove Admin Panel Entries


function remove_menus ()
{
	// Dashboard:
	// remove_menu_page ('index.php');

	// Posts:
	remove_menu_page ('edit.php');

	// Media:
	// remove_menu_page ('upload.php');

	// Pages:
	// remove_menu_page ('edit.php?post_type=page');

	// Comments:
	remove_menu_page ('edit-comments.php');

	// Appearance:
	// remove_menu_page ('themes.php');

	// Plugins:
	// remove_menu_page ('plugins.php');

	// Users:
	// remove_menu_page ('users.php');

	// Tools:
	// remove_menu_page ('tools.php');

	// Settings:
	// remove_menu_page ('options-general.php');
}

add_action ('admin_menu', 'remove_menus');


//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


// http://www.wpexplorer.com/wordpress-tinymce-tweaks/


function tiny_mce_options ($init)
{
	$default_colours = '
	"A00876", "lila", "444444", "grau"
	';
	// $custom_colours = '';
	// $init['textcolor_map'] = '['.$default_colours.','.$custom_colours.']';
	$init['textcolor_map'] = '['.$default_colours.']';
	// $init['textcolor_rows'] = 4; // expand colour grid to 6 rows
	return $init;
}

add_filter ('tiny_mce_before_init', 'tiny_mce_options');



function tuts_mce_before_init ($settings)
{
	$style_formats = array(
		array(
			'title' => 'Headline',
			'block' => 'h1',
			'classes' => ''
		),
		array(
			'title' => 'Headline in Copy',
			'block' => 'h2',
			'classes' => ''
		),
		array(
			'title' => 'Copy Text',
			'block' => 'p',
			'classes' => ''
		),
		array(
			'title' => 'Impressum',
			'block' => 'p',
			'classes' => 'small'
		)
	);

	$settings['style_formats'] = json_encode ($style_formats);

	return $settings;
}

add_filter ( 'tiny_mce_before_init', 'tuts_mce_before_init' );



// function wwiz_mce_inits($initArray){
//     // $initArray['height'] = '600px';
//     return $initArray;
// }
// add_filter('tiny_mce_before_init', 'wwiz_mce_inits');



if ( ! function_exists( 'wpex_mce_text_sizes' ) ) {
	function wpex_mce_text_sizes( $initArray ){
		$initArray['fontsize_formats'] = "9px 10px 12px 13px 14px 16px 18px 21px 24px 28px 32px 36px";
		return $initArray;
	}
}
add_filter( 'tiny_mce_before_init', 'wpex_mce_text_sizes' );


//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::  custom mime-types


function custom_upload_mimes ($existing_mimes=array())
{
	$existing_mimes['vcf'] = 'text/x-vcard';
	return $existing_mimes;
}

add_filter ('upload_mimes', 'custom_upload_mimes');



//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: PHP LOGGER


require_once (__DIR__ . '/PhpConsole/__autoload.php');
$handler = PhpConsole\Handler::getInstance ();
$handler->start ();
PhpConsole\Helper::register ();



// Call debug from PhpConsole\Connector (if you don't use PhpConsole\Handler in your project)
// PhpConsole\Connector::getInstance ()->getDebugDispatcher ()->dispatchDebug ('called from debug dispatcher without tags');

// Call debug from global PC class-helper (most short & easy way)
// required to register PC class in global namespace, must be called only once
// PC::debug ('called from PC::debug ()', 'db');
// PC::db ('called from PC::__callStatic ()'); // means "db" will be handled as debug tag


// Debug some mixed variable

// class DebugExample {

//     private $privateProperty = 1;
//     protected $protectedProperty = 2;
//     public $publicProperty = 3;
//     public $selfProperty;

//     public function __construct () {
//         $this->selfProperty = $this;
//     }

//     public function someMethod () {
//     }
// }


// PC::debug (array (
//     'null' => null,
//     'boolean' => true,
//     'longString' => '11111111112222222222333333333344444444445',
//     'someObject' => new DebugExample (),
//     'someCallback' => array (new DebugExample (), 'someMethod'),
//     'someClosure' => function () {
//     },
//     'someResource' => fopen (__FILE__, 'r'),
//     'manyItemsArray' => array (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11),
//     'deepLevelArray' => array (1 => array (2 => array (3))),
//));



//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// function add_ck_json_controller ($controllers)
// {
// 	$controllers[] = 'ck_json';
// 	return $controllers;
// }

// add_filter ('json_api_controllers', 'add_ck_json_controller');



// function set_ck_json_controller_path ()
// {
// 	return get_stylesheet_directory ()."/ck_json.php";
// }

// add_filter ('json_api_ck_json_controller_path', 'set_ck_json_controller_path');
