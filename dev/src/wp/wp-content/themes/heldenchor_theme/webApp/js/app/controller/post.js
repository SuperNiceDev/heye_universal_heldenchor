/**
* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* @js class 		PostTest
* @author 		philipp.neumann@supernice-dev.com
* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/



define
(
	[
		"app/controller/restServiceController"
	],


	//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	function (
			RestServiceController
		)
	{
		var PostTest = Backbone.Model.extend (
		{

			name: "PostTest",
			bbClassName: "PostTest",


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			initialize: function initialize (p_options)
			{
				// _.bindAll (this, "_dataLoadedHandler");
				//
				// var config = {
				// 	method: "POST"
				// 	, data: {
				// 		"postId": 278
				// 		,"field_key": "comments"
				// 	}
				// };
				//
				// this._restServiceController = new RestServiceController ({"config": config, "isUrlQuery": false });
				// this._restServiceController.on (RestServiceController.DATA_LOAD_EVENT, this._dataLoadedHandler);
				// this._restServiceController.loadData (p_options.config.siteRoot + "/wp-json/restservice/update_post/");
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_dataLoadedHandler: function _dataLoadedHandler (p_event)
			{
				// console.info (p_event);
			}


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		});


		return PostTest;
	}
);
