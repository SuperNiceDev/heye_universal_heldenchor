/**
* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* @js class 		AppModel
* @author 		p.neumann@supernice-dev.com
* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/



define
(
	[
		"app/controller/router",
	],


	//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

			
	function (
			Router
		)
	{
		var AppModel = Backbone.Model.extend (
		{

			name: "AppModel",
			bbClassName: "AppModel",

			INIT_EVENT: "AppModel.INIT_EVENT",
			UPDATE_EVENT: "AppModel.UPDATE_EVENT",

			data: null,


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			initialize: function initialize ()
			{
				// LogCenter.log (this, "");
				this._eventBus = SNDLib.View.EventBus;
				this._siteRoot = "";

				// _.bindAll (this, "_routerChangeRequestEventHandler");
				_.bindAll (this, "_routerUrlUpdateEventHandler");
			},



			addEvents: function addEvents ()
			{
				// this._eventBus.on (this._eventBus.XY_EVENT, this._routerChangeRequestEventHandler);
				this._eventBus.on (Router.UPDATE_EVENT, this._routerUrlUpdateEventHandler);

				if (this.options.config)
				{
					if (this.options.config.siteRoot)
					{
						this._siteRoot = this.options.config.siteRoot;
					}
				}
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			// _routerChangeRequestEventHandler: function _routerChangeRequestEventHandler (p_event)
			// {
			// 	LogCenter.log (this, "p_event: " + p_event);
			// },



			_routerUrlUpdateEventHandler: function _routerUrlUpdateEventHandler (p_path)
			{
				// Wordpress -> Einstellungen -> Lesen: Startseite zeigt -> Deine letzten Beiträge
				// Wordpress Polylang -> Einstellungen: Browsersprache erkennen -> AUS
				
				// LogCenter.log (this, "p_path: " + p_path);
				p_path = p_path || this._getUserLanguage ();

				if (! this._isRouteChanged)
				{
					var pathSplit = this.getPathSplit (p_path);
					pathSplit[0] = this._getUserLanguage ();
					p_path = pathSplit.join ("/");
				}

				this.setState (p_path);

				this._isRouteChanged = true;
			},



			_getUserLanguage: function _getUserLanguage ()
			{
				var localStorage = localStorage || window.localStorage;
				var localStorageLang = "";
				if (localStorage) localStorageLang = localStorage.getItem ("vr.black.space:::userSelectedLanguage");

				this._userSelectedLanguage = "de";
				if (localStorageLang === "de" || localStorageLang === "en")
				{
					this._userSelectedLanguage = localStorageLang;
				}

				var browserLang = navigator.language || navigator.userLanguage;
				if (browserLang.indexOf ("de") >= 0)
				{
					browserLang = "de";
				}
				else
				{
					browserLang = "en";
				}

				return this._userSelectedLanguage || browserLang;
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			setState: function setState (p_path)
			{
				p_path = this.getCleanPath (p_path);
				// LogCenter.log (this, "urlSplit: " + urlSplit);

				var urlSplit = this.getPathSplit (p_path);
				var state = this.data.state = this.data.state || {};
				state.language = urlSplit[0];

				if (urlSplit.length === 1)
				{
					state.url = p_path + "/home";
					state.data =  this.getObjectFromUrl (this.data, state.url);
					var breakPoint = 0;
				}
				else
				{
					var tmpData = this.data;
					var tmpUrl = "";

					for (var k = 0, len = urlSplit.length; k < len; k ++)
					{
						tmpUrl = tmpUrl + urlSplit[k] + "/";
						tmpData = tmpData[urlSplit[k]];

						if (tmpData)
						{
							state.data = tmpData;
							state.url = tmpUrl;
						}
						else
						{
							LogCenter.log (this, "no data at:       " + tmpUrl, LogCenter.LEVEL_WARNING);
							LogCenter.log (this, "took data from: " + state.url, LogCenter.LEVEL_WARNING);

							Router.setPath (state.url.replace (/\/?$/, '/'));
							return;
						}
					}
				}

				
				// LogCenter.log (this, "this.data.state.url:" + this.data.state.url);
				// console.info (this.data);
				

				this._setLocalStorage (state.language);
				Router.setPath (p_path.replace (/\/?$/, '/'), false);
				this.update ();
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_setLocalStorage: function _setLocalStorage (p_language)
			{
				var language = this.data.state.language;
				// LogCenter.log (this, "language:" + language);

				var localStorage = localStorage || window.localStorage;
				if (localStorage)
				{
					localStorage.setItem ("snd_wp_template:::userSelectedLanguage", p_language);
				}
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			getCleanPath: function getCleanPath (p_path)
			{
				if (! p_path) return "";
				
				// http://jsfiddle.net/X6jzs/57/
				// http://jsfiddle.net/X6RPb/
				p_path = p_path.replace (/(\/\/+)/g, '/'); // remove double slash
				p_path = p_path.replace (/^\//,""); // remove first slash
				p_path = p_path.replace (/\/+$/, ""); // remove last slash

				return p_path;
			},



			getPathSplit: function getPathSplit (p_path)
			{
				p_path = this.getCleanPath (p_path);
				var urlSplit = p_path.split ("/");
				// LogCenter.log (this, "urlSplit: " + urlSplit);

				return urlSplit;
			},



			getObjectFromUrl: function getObjectFromUrl (p_obj, p_path)
			{
				if (! p_obj || ! p_path) return;

				p_path = p_path.replace (this._siteRoot, "");
				var urlSplit = this.getPathSplit (p_path)
				var data = p_obj;
				var tmpData = {};
				// LogCenter.log (this, "urlSplit: " + urlSplit);

				for (var k = 0, len = urlSplit.length; k < len; k ++)
				{
					tmpData = data[urlSplit[k]];

					if (tmpData)
					{
						data = tmpData;
					}
					else
					{
						LogCenter.log (this, "No Data at: " + p_path, LogCenter.LEVEL_WARNING);
						return;
					}
				}

				return data;
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			init: function init ()
			{
				this._eventBus.trigger (this.INIT_EVENT, this.data);
			},



			update: function update ()
			{
				this._eventBus.trigger (this.UPDATE_EVENT, this.data);
			}


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		});

  
		return new AppModel ();
	}
);