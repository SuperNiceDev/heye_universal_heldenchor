/**
* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* @js class 		HeaderController
* @author 		philipp.neumann@supernice-dev.com
* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/



define
(
	[
		"app/model/appModel"
	],


	//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	function (
			AppModel
		)
	{
		var HeaderController = Backbone.Model.extend (
		{

			name: "HeaderController",
			bbClassName: "HeaderController",


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			initialize: function initialize (p_options)
			{
				this.options = p_options;
				// LogCenter.log (this, "");

				_.bindAll (this, "_modelUpdateHandler");

				this._eventBus = SNDLib.View.EventBus;
				this._eventBus.on (AppModel.UPDATE_EVENT, this._modelUpdateHandler);
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_modelUpdateHandler: function _modelUpdateHandler (p_event)
			{
				// LogCenter.log (this, "p_event: " + p_event, LogCenter.LEVEL_DEBUG);

				var acf = p_event.state.data._post.acf;


				// HEADER:
				var title = this.options.config.siteTitle + " | " + acf.header_title;
				$("title").text (title);

				var description = acf.header_description;
				$("meta[name=description]").attr ("content", description);

				var keywords = acf.header_keywords;
				$("meta[name=keywords]").attr ("content", keywords);


				// OPEN GRAPH:
				// var url = this.options.config.siteDomain + "/" + p_event.url + "/";
				// $("meta[property='og:url']").attr ("content", url);
				// $("meta[property='og:type']").attr ("content", "website");
				// $("meta[property='og:title']").attr ("content", title);
				// $("meta[property='og:description']").attr ("content", description);
				// $("meta[property='og:image']").attr ("content", acf.sharing_img);
			}


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		});


		return HeaderController;
	}
);