/**
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * @js class 		Btn
 * @author 		philipp.neumann@heye.de
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */



define (
	[
	],


	//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	function (	
		)
	{
		var Btn = SNDLib.View.Btn.extend (
		{

			// name: "Btn",
			bbClassName: "Btn",

			id: "",
			className: "",

			_isClicked: false,


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_onInitialize: function _onInitialize ()
			{
				this._super ();
			},



			render: function render ()
			{
				this._super ();
			},


			//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			setClicked: function setClicked ()
			{
				if (this._isClicked) return;

				this._mouseOverEventHandler ();

				this._isClicked = true;
			},



			setUnclicked: function setUnclicked ()
			{
				if (! this._isClicked) return;

				this._mouseOutEventHandler ();

				this._isClicked = false;
			},


			//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_mouseOverEventHandler: function _mouseOverEventHandler (p_event)
			{
				if (this._isClicked) return;

				// LogCenter.log (this, "p_event" + p_event, LogCenter.LEVEL_DEBUG);

				TweenLite.killTweensOf (this.$el);
				TweenLite.to (this.$el, 0.3, {"delay": 0.0, "css": {className: "+=btn-hover-state"}, "ease": Cubic.easeInOut });
			},



			_mouseOutEventHandler: function _mouseOutEventHandler (p_event)
			{
				if (this._isClicked) return;

				// LogCenter.log (this, "p_event" + p_event, LogCenter.LEVEL_DEBUG);

				TweenLite.killTweensOf (this.$el);
				TweenLite.to (this.$el, 0.3, {"delay": 0.0, "css": {className: "-=btn-menu-bottom-hover-state"}, "ease": Cubic.easeInOut });
			}


			//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		});


		return Btn;
	}
);