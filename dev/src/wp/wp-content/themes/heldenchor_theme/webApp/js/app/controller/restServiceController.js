/**
* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* @js class 		RestServiceController
* @author 		philipp.neumann@supernice-dev.com
* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/



define
(
	[
	],


	//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	function (
		)
	{
		var RestServiceController = Backbone.Model.extend (
		{

			name: "RestServiceController",
			bbClassName: "RestServiceController",


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			initialize: function initialize (p_options)
			{
				this.options = p_options || {};
				// LogCenter.log (this, "");

				_.bindAll (this, "_ajaxFailHandler", "_ajaxDoneHandler");

				this._eventBus = SNDLib.View.EventBus;
				this._config = this.options.config || {};

				if (this.options.url)
				{
					this.loadData (this.options.url);
				}
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			loadData: function loadData (p_url)
			{
				if (this.options.isUrlQuery === true) p_url += window.location.search;
				LogCenter.log (this, "p_url: " + p_url);

				this._startTime = new Date ();
				this._config.url = p_url;
				var  _this = this;

				// https://coderwall.com/p/je3uww/get-progress-of-an-ajax-request
				this._config.xhr = function ()
				{
					var xhr = $.ajaxSettings.xhr ();
					xhr.onprogress = function (e)
					{
						if (e.lengthComputable)
						{
							// console.log ("Download >>>>>>>>>>>>");
							// console.log (e.loaded / e.total);
						}
					};
					xhr.upload.onprogress = function (e)
					{
						if (e.lengthComputable)
						{
							// console.log ("Upload >>>>>>>>>>>>");
							// console.log (e.loaded / e.total);
							_this.trigger (RestServiceController.DATA_UPLOAD_PROGRESS_EVENT, {"progress": e.loaded / e.total});
						}
					};
					return xhr;
				}

				// https://api.jquery.com/jQuery.ajax/
				var ajax = $.ajax (this._config);
				ajax.fail (this._ajaxFailHandler);
				ajax.done (this._ajaxDoneHandler);
			},



			_ajaxFailHandler: function _ajaxFailHandler (p_event)
			{
				LogCenter.log (this, "------------------------------------------- p_event: " + p_event, LogCenter.LEVEL_ERROR);
				console.info (p_event);
				LogCenter.log (this, "-------------------------------------------");
			},



			_ajaxDoneHandler: function _ajaxDoneHandler (p_event)
			{
				var endTime = new Date ();
				var timeDiff = (endTime - this._startTime) / 1000;
				var data;
				var type = $.type (p_event);

				if (type === "string")
				{
					data = JSON.parse (p_event);
				}
				else
				{
					data = p_event;
				}

				LogCenter.log (this, "-------------------------------------------");
				console.info (data);
				LogCenter.log (this, "------------------------------------------- request time: " + timeDiff + " sec");

				this.trigger (RestServiceController.DATA_LOAD_EVENT, data);
				// this._eventBus.trigger (RestServiceController.DATA_LOAD_EVENT, data);
			}


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		});


		RestServiceController.DATA_UPLOAD_PROGRESS_EVENT = "RestServiceController.DATA_UPLOAD_PROGRESS_EVENT";
		RestServiceController.DATA_LOAD_EVENT = "RestServiceController.DATA_LOAD_EVENT";


		return RestServiceController;
	}
);
