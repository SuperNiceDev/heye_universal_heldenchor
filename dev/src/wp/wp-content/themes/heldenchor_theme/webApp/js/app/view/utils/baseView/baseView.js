/**
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * @js class 		BaseView
 * @author 		p.neumann@supernice-dev.com
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */



define (
	[
		"app/model/appModel",

		"text!app/view/utils/baseView/baseView.htm",
		"less!app/view/utils/baseView/baseView.less"
	],


	//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	function (
			AppModel,

			p_htmlTpl,
			p_less
		)
	{
		var BaseView = SNDLib.View.BaseView.extend (
		{

			name: "BaseView",
			bbClassName: "BaseView",

			className: "baseView",
			id: "",
			
			_tweenDur: 0.5,


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_onInitialize: function _onInitialize ()
			{
				// LogCenter.log (this, "", LogCenter.LEVEL_DEBUG);

				_.bindAll (this, "_afterShow");
				_.bindAll (this, "_afterHide");

				// _.bindAll (this, "_hrefMouseOverHandler");
				// this.isShow = false;
				// TweenLite.set (this.$el, {"alpha": 0 });


				this._AppModel = AppModel;
				
				_.bindAll (this, "_modelUpdateHandler");
				this._eventBus.on (AppModel.UPDATE_EVENT, this._modelUpdateHandler);


				/**
					===> Add class name to child class

					this.options.addClass = "navView";
					this._super ();
				**/
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_modelUpdateHandler: function _modelUpdateHandler (p_event)
			{
				// LogCenter.log (this, "p_event: " + p_event);

				this._data = p_event;
				this._state = p_event.state;

				if (this._language !== p_event.state.language)
				{
					this._language = p_event.state.language;
					this._modelLanguageUpdateHandler (p_event);
				}

				this._modelStateUpdateHandler (p_event);
			},



			_modelLanguageUpdateHandler: function _modelLanguageUpdateHandler (p_event)
			{
				LogCenter.log (this, "BaseView");
			},



			_modelStateUpdateHandler: function _modelStateUpdateHandler (p_event)
			{
				LogCenter.log (this, "BaseView");
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			render: function render ()
			{
				// if (this.isRendered) return;
				// LogCenter.log (this, "");

				this.$el.html (_.template (p_htmlTpl));

				return this;
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_renderUnderline: function _renderUnderline ()
			{
				this.$el.find ("a").addClass ("underlineCtn");
				this.$el.find (".underlineCtn").append ("<div class='underline'></div>");
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_getObjsAsArray: function _getObjsAsArray (p_obj)
			{
				if (! p_obj._postChildrenOrder) return false;

				var arr = [];
				var idArr = p_obj._postChildrenOrder;
				for (var k = 0, len = idArr.length; k < len; k ++)
				{	
					// var id = idArr[k];
					// var obj = p_obj[id];
					// arr[k] = obj;
					arr[k] = p_obj[idArr[k]];
					arr[k].slug = idArr[k];
					arr[k].permalink = this.options.config.siteRoot + this._language + "/" + idArr[k];
				}

				return arr;
			},



			_shuffleArr: function _shuffleArr (p_array)
			{
				for (var k = p_array.length - 1; k > 0; k --)
				{
					var k2 = Math.floor (Math.random () * (k + 1));
					var item = p_array[k];
					p_array[k] = p_array[k2];
					p_array[k2] = item;
				}

				return p_array;
			},


			//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_onShow: function _onShow ()
			{
				TweenLite.to (this.$el, this._tweenDur, {"alpha": 1, "onComplete": this._afterShow });
			},



			_afterShow: function _afterShow ()
			{
			},


			//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_onHide: function _onHide ()
			{
				TweenLite.to (this.$el, this._tweenDur, {"alpha": 0, "onComplete": this._afterHide });
			},



			_afterHide: function _afterHide ()
			{
				// this.destroy ();
			}


			//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		});


		return BaseView;
	}
);