/**
* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* @js class 		WebApp
* @author 		p.neumann@supernice-dev.com
* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/



require.config
({
	// baseUrl: "",
	// suppress: { nodeShim: true },
	siteRoot: "../../",
	paths:
	{
		// ::: Syntax for using CDN (Content Delivery Network) with local file fallback
		// libName: ["CDN src", "LOCAL src"],


		// ::: Libs required by Backbone
		underscore: "lib/underscore/underscore",
		jquery: "lib/jquery/jquery",

		backbone: "lib/backbone/backbone",
		backboneSuper: "lib/backbone/backbone-super",

		text: 'lib/require/text',
		// css: 'lib/require/css.min',


		// ::: 3rd Party lib
		history: "lib/history/history",
		greensockTweenLite: "lib/greensock/uncompressed/TweenLite",
		greensockTweenMax: "lib/greensock/uncompressed/TweenMax",
		greensockCSSPlugin: "lib/greensock/uncompressed/plugins/CSSPlugin",
		greensockScrollPlugin: "lib/greensock/uncompressed/plugins/ScrollToPlugin",
		// greensockSplitText: "lib/greensock/uncompressed/SplitText",

		uaParser: "lib/ua-parser/ua-parser",
		canvid: "lib/canvid/canvid",
		spinjs: "lib/spinjs/spin",
		validatejs: "lib/validatejs/validate",


		// ::: SuperNice Dev lib
		// lib/snd/snd_js_backbonejs-ext/
		// ../../../../../../../../../../../snd/snd_js_backbonejs-ext/lib/
		sndLibNamespace: "../../../../../../../../../../../snd/snd_js_backbonejs-ext/lib/snd-lib-namespace",
		sndLibUtils: "../../../../../../../../../../../snd/snd_js_backbonejs-ext/lib/snd-lib-utils",
		sndLibViewBaseView: "../../../../../../../../../../../snd/snd_js_backbonejs-ext/lib/snd-lib-view-baseView",
		sndLibViewBtn: "../../../../../../../../../../../snd/snd_js_backbonejs-ext/lib/snd-lib-view-btn",
		sndLibViewBtnNoTabDelay: "../../../../../../../../../../../snd/snd_js_backbonejs-ext/lib/snd-lib-view-btnNoTabDelay",
		sndLibViewLogView: "../../../../../../../../../../../snd/snd_js_backbonejs-ext/lib/snd-lib-view-logView",
		sndLibViewResizeImg: "../../../../../../../../../../../snd/snd_js_backbonejs-ext/lib/snd-lib-view-resizeImg",
		sndLibViewScroll: "../../../../../../../../../../../snd/snd_js_backbonejs-ext/lib/snd-lib-view-scroll",
		sndLibViewSlider: "../../../../../../../../../../../snd/snd_js_backbonejs-ext/lib/snd-lib-view-slider",
		sndLibViewSliderExt: "../../../../../../../../../../../snd/snd_js_backbonejs-ext/lib/snd-lib-view-sliderExt",
		sndLibViewSliderExtStopper: "../../../../../../../../../../../snd/snd_js_backbonejs-ext/lib/snd-lib-view-sliderExtStopper",
		sndLibViewSliderScrollbar: "../../../../../../../../../../../snd/snd_js_backbonejs-ext/lib/snd-lib-view-sliderScrollbar",
		sndLibViewTouch: "../../../../../../../../../../../snd/snd_js_backbonejs-ext/lib/snd-lib-view-touch",
		sndLibViewUtils: "../../../../../../../../../../../snd/snd_js_backbonejs-ext/lib/snd-lib-view-utils"
	},


	map: {
		"*": {
			"css": "lib/require/require-css/css",
			"less": "lib/require/require-less/less"
		}
		// ,
		// "greensockCSSPlugin": {
		// 	"TweenLite": "greensockTweenLite"
		// }
	},


	// ../../../../../../../../../snd_js_backbonejs-ext/template/src/webApp/js/lib/


	shim:
	{
		"jquery":
		{
			exports: "jquery"
		},

		"backbone":
		{
			deps: ["underscore", "jquery"],
			exports: "Backbone"
		},

		"backboneSuper":
		{
			deps: ["backbone"],
			exports: "BackboneSuper"
		},



		// ::: 3rd Party Libs

		// "greensockCSSPlugin":
		// {
		// 	deps: ["greensockTweenLite"]
		// },

		// "greensockSplitText":
		// {
		// 	deps: ["greensockTweenMax"]
		// },

		// "spinjs":
		// {
		// 	exports: "Spinner"
		// },



		// ::: SuperNice Dev lib

		"sndLibNamespace":
		{
			deps: ["backboneSuper"],
			// exports: "SNDLib"
		},

		"sndLibUtils":
		{
			deps: ["sndLibNamespace"],
			// exports: "SNDLib"
		},

		"sndLibViewBaseView":
		{
			deps: ["sndLibNamespace"],
			// exports: "SNDLib"
		},

		"sndLibViewBtn":
		{
			deps: ["sndLibViewBaseView"],
			// exports: "SNDLib"
		},

		"sndLibViewBtnNoTabDelay":
		{
			deps: ["sndLibViewBaseView"],
			// exports: "SNDLib"
		},

		"sndLibViewLogView":
		{
			deps: ["sndLibViewBaseView"],
			// exports: "SNDLib"
		},

		"sndLibViewResizeImg":
		{
			deps: ["sndLibViewBaseView"],
			// exports: "SNDLib"
		},

		"sndLibViewScroll":
		{
			deps: ["sndLibViewBaseView"],
			// exports: "SNDLib"
		},

		"sndLibViewSlider":
		{
			deps: ["sndLibViewBaseView"],
			// exports: "SNDLib"
		},

		"sndLibViewSliderExt":
		{
			deps: ["sndLibViewSlider"],
			// exports: "SNDLib"
		},

		"sndLibViewSliderExtStopper":
		{
			deps: ["sndLibViewSliderExt"],
			// exports: "SNDLib"
		},

		"sndLibViewSliderScrollbar":
		{
			deps: ["sndLibViewBaseView"],
			// exports: "SNDLib"
		},

		"sndLibViewTouch":
		{
			deps: ["sndLibViewBaseView"],
			// exports: "SNDLib"
		},

		"sndLibViewUtils":
		{
			deps: ["sndLibViewBaseView"],
			// exports: "SNDLib"
		}
	}
});


//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


require
(
	[
		// "underscore",
		// "jquery",
		"backbone",
		"backboneSuper",
		"text",
		// "css",

		"history",
		"greensockTweenMax",
		// "greensockTweenLite",
		// "greensockCSSPlugin",
		// "greensockScrollPlugin",
		// "greensockSplitText",
		// "spinjs",
		"validatejs",

		"sndLibNamespace",
		"sndLibUtils",
		"sndLibViewBaseView",
		"sndLibViewBtn",
		// "sndLibViewBtnNoTabDelay",
		// "sndLibViewLogView",
		"sndLibViewResizeImg",
		"sndLibViewScroll",
		// "sndLibViewSlider",
		// "sndLibViewSliderExt",
		// "sndLibViewSliderExtStopper",
		// "sndLibViewSliderScrollbar",
		"sndLibViewTouch",
		"sndLibViewUtils"
	],


	function ()
	{
		// LogCenter.log (this, "webApp.js :: required js data loaded to global object");

		require
		(
			[
				"app/app"
			],

			function (App)
			{
				// LogCenter.log (this, "webApp.js :: app.js loaded witdh all dependencies");

				var app = new App ({"el": ".webApp",  "config": webAppConfig });
			}
		);
	}
);



//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
