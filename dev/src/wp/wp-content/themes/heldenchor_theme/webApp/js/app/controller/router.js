/**
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * @js class 		Router
 * @author 		p.neumann@supernice-dev.com
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */


// ### html5 pushstate (history) support:

// <ifModule mod_rewrite.c>
// 	RewriteEngine On

// 	RewriteBase /URL/URL/

// 	RewriteCond %{REQUEST_FILENAME} !-f
// 	RewriteCond %{REQUEST_FILENAME} !-d
// 	RewriteCond %{REQUEST_URI} !index
// 	RewriteRule (.*) index.php [L,QSA]
// </ifModule>


// ### for html
// <base href="/snd/snd_js_backbonejs-ext/template/src/" />


define (
	[
	],


	//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	function (
		)
	{
		var Router = Backbone.Router.extend (
		{

			name: "Router",
			bbClassName: "Router",

			UPDATE_EVENT: "Router.UPDATE_EVENT",


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			initialize: function initialize ()
			{
				//LogCenter.log (this, "name:" + this.name);

				this._eventBus = SNDLib.View.EventBus;

				this._routerRootPath = "";
				this._siteRoot = "";
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			startRouting: function startRouting ()
			{
				// LogCenter.log (this, "");

				this.route ("*default", "default");
				this.on ("route:default", this._defaultRouteEvent, this);

				this.route ("*p_path", "routeUpdateEvent");
				this.on ("route:routeUpdateEvent", this._routeUpdateEventHandler, this);

				if (this.options)
				{
					if (this.options.el)
					{
						_.bindAll (this, "_aClickEventHandler");
						$(this.options.el).on ("click", "a", this._aClickEventHandler);
					}

					if (this.options.config)
					{
						if (this.options.config.routerRootPath)
						{
							this._routerRootPath = this.options.config.routerRootPath;
						}

						if (this.options.config.siteRoot)
						{
							this._siteRoot = this.options.config.siteRoot;
						}
					}
				}

				Backbone.history.start ({"pushState": true, "root": this._routerRootPath });
			},



			stopRouting: function stopRouting ()
			{
				Backbone.history.stop ();

				this.off ("route:default", this._defaultRouteEvent);
				this.off ("route:routeUpdateHandler", this._routeUpdateEventHandler, this);
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_defaultRouteEvent: function _defaultRouteEvent (p_event)
			{
				LogCenter.log (this, "p_event: " + p_event);
			},



			_routeUpdateEventHandler: function _routeUpdateEventHandler (p_path)
			{
				// LogCenter.log (this, "p_path: " + p_path, LogCenter.LEVEL_DEBUG);

				this._eventBus.trigger (this.UPDATE_EVENT, p_path);

				try
				{
					ga ("send", "pageview", {"page": p_path });
				}
				catch (p_error)
				{

				}
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_aClickEventHandler: function _aClickEventHandler (p_event)
			{
				var href = $(p_event.currentTarget).attr ("href");
				var target = $(p_event.currentTarget).attr ("target");
				if (! href) return;


				var isHttp = href.indexOf ("http://");
				var isHttps = href.indexOf ("https://");

				if (isHttp === -1 && isHttps === -1)
				{
					href = this._siteRoot + "/" + href;
				}


				var internalLink = href.indexOf (this._siteRoot);

				if (internalLink > -1 && target !== "_blank")
				{
					p_event.preventDefault ();
					var path = href.replace (this._siteRoot, "");
					path = path.replace (/(\/\/+)/g, '/');
					path = path.replace (/^\//,"");

					// LogCenter.log (this, "path: " + path, LogCenter.LEVEL_DEBUG);

					this.setPath (path);
				}
				else
				{
					$(p_event.currentTarget).attr ("target", "_blank");
				}


				// var sameDomain = href.indexOf (this._siteRoot);
				// var pdf = href.indexOf (".pdf");
				// var mailTo = href.indexOf ("mailto:");

				// if (sameDomain === 0 && pdf < 0)
				// {
				// 	p_event.preventDefault ();
				// 	var path = href.replace (this._siteRoot, "");
				// 	path = path.replace (/(\/\/+)/g, '/');
				// 	path = path.replace (/^\//,"");

				// 	// LogCenter.log (this, "path: " + path, LogCenter.LEVEL_DEBUG);

				// 	this.setPath (path);
				// }
				// else if (mailTo >= 0)
				// {
				// }
			},
			


			setPath: function setPath (p_path, p_trigger)
			{
				// LogCenter.log (this, "p_path: " + p_path, LogCenter.LEVEL_DEBUG);
				if (p_trigger === undefined) p_trigger = true;
				this.navigate (p_path, {"trigger": p_trigger });
			}


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		});


		return new Router ();
	}
);