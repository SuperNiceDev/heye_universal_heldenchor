/**
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * @js class 		UploadView
 * @author 		p.neumann@supernice-dev.com
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 */



define (
	[
		"text!app/view/uploadView/uploadView.htm",
		"less!app/view/uploadView/uploadView.less",

		"app/controller/restServiceController"
		// "validatejs"
	],


	//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	function (
			p_htmlTpl,
			p_css,

			RestServiceController
			// validate
		)
	{
		var UploadView = SNDLib.View.BaseView.extend (
		{

			name: "UploadView",
			bbClassName: "UploadView",

			id: "uploadView-id",
			className: "uploadView",


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_onInitialize: function _onInitialize ()
			{
				// LogCenter.log (this, "");

				_.bindAll (this, "_open");
				_.bindAll (this, "_close");
				_.bindAll (this, "_upload");
				_.bindAll (this, "_dataProgressHandler");
				_.bindAll (this, "_dataUploadedHandler");

				this.render ();
			},



			render: function render ()
			{
				// LogCenter.log (this, "");

				this.$el.html (_.template (p_htmlTpl));

				this._eventBus.on ("OPEN_FORM", this._open);

				this._bg = this.$el.find (".blk");
				this._bg.on ("click", this._close);
				this.$el.find (".closeAction").on ("click", this._close);

				this._forename = this.$el.find ("#_forename");
				this._surname = this.$el.find ("#_surename");
				this._email = this.$el.find ("#_email");
				this._place = this.$el.find ("#_place");
				this._file = this.$el.find ("#_file");
				this._uploadStatus = this.$el.find (".uploadStatus");

				this.$el.find ("#_submit").on ("click", this._upload);

				return this;
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_open: function _open (p_event)
			{
				this._id = p_event.currentTarget.id;
				if (this._id === "_newsletter") this._id = "_howTo";
				// LogCenter.log (this, "_id " + this._id);
				this.$el.addClass ("show " + this._id);
			},



			_close: function _close (p_event)
			{
				this.$el.removeClass ("show " + this._id);
				this.$el.removeClass ("finshed");
				this._clearForm ();
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_upload: function _upload ()
			{
				if (this._checkForm ().success === false) return;

				// this._formData.append ('image', $('input[type=file]')[0].files[0]);
				this._formData = new FormData (this.$el.find ("form")[0]);
				this._formData.append ('postId', 278);
				this._formData.append ('field_key', "videos");
				this._formData.append ('formType', this._id);

				var config = {
					method: "POST"
					// ,data: {
					// 	"postId": 278
					// 	,"field_key": "comments"
						// ,"name": this._name
					// 	,"text": this._text[0]
					// 	// ,"videoFile": this._videoFile[0]
					// }
					,data: this._formData
					// ,dataType: 'json'
					,cache: false
					,processData: false
					,contentType: false
				};

				this._restServiceController = new RestServiceController ({"config": config });
				this._restServiceController.on (RestServiceController.DATA_UPLOAD_PROGRESS_EVENT, this._dataProgressHandler);
				this._restServiceController.on (RestServiceController.DATA_LOAD_EVENT, this._dataUploadedHandler);
				this._restServiceController.loadData (this.options.config.siteRoot + "/wp-json/restservice/update_post/");
			},


			//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_checkForm: function _checkForm ()
			{
				var obj = {"success": true };

				// https://validatejs.org/#validators-email
				var emailTestProps = {from: {email: {message: "Bitte Email kontrollieren"}}};
				var test = validate ({from: this._email.val ()}, emailTestProps) || {from: []};
				// console.log (test);
				var message = "";
				if (test.from.length)
				{
					obj.success = false;
					var message = test.from[0].replace ("From", "");
					// console.log (message);
				}


				this._forename.val ();
				this._surname.val ();
				this._place.val ();


				// console.log (this._file[0].files[0]);
				// console.log ("this._id: " + this._id);
				if (this._id === "_videoUpload" && ! this._file[0].files[0])
				{
					obj.success = false;
				}


				var checkBoxAGB = this.$el.find ("input[name=checkBoxAGB]:checked").val ();
				// console.log ("checkBoxAGB: " + checkBoxAGB);
				if (checkBoxAGB !== "on")
				{
					obj.success = false;
				}


				console.log ("obj.success: " + obj.success);
				return obj;
			},



			_clearForm: function _clearForm ()
			{
				this._forename.val ("");
				this._surname.val ("");
				this._email.val ("");
				this._place.val ("");
				this.$el.find ("input[type=checkbox]").prop ('checked', false);
				this._uploadStatus.text ("");
			},


			//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_dataProgressHandler: function _dataProgressHandler (p_event)
			{
				if (p_event.progress)
				{
					var progress = parseInt (p_event.progress * 100);
					// var progress = p_event.progress * 100;
					// LogCenter.log (this, "progress: " + progress);
					this._uploadStatus.text ("Forschritt: " + progress + "%");
				}
			},



			_dataUploadedHandler: function _dataUploadedHandler (p_event)
			{
				LogCenter.log (this, ">>>>>>>>>>>>>>>>");
				console.log (p_event);
				// this._restServiceController.off (RestServiceController.DATA_UPLOAD_PROGRESS_EVENT);
				// this._restServiceController.off (RestServiceController.DATA_LOAD_EVENT);

				this.$el.addClass ("finshed");
				this._clearForm ();
			}


			//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		});


		// UploadView. = "UploadView.TOUCH_VIEW_EVENT";


		return UploadView;
	}
);
