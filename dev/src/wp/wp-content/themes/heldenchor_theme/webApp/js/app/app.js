/**
* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* @js class 		App
* @author 		p.neumann@supernice-dev.com
* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/


define
(
	[
		"less!app/reset.less",
		"text!app/app.htm",
		"less!app/app.less",
		"less!app/app_header_footer.less",
		"less!app/app_teaser.less",
		"less!app/app_desc.less",
		"less!app/app_song.less",

		"app/view/uploadView/uploadView",

		"uaParser",
		"spinjs"
	],


	//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


	function (
			p_resetLess,
			p_html,
			p_less_app,
			p_less_header_footer,
			p_less_teaser,
			p_less_desc,
			p_less_song,

			UploadView,

			UAParser,
			SpinJS
		)
	{
		var App = SNDLib.View.BaseView.extend (
		{

			version: "0.0.1",
			bbClassName: "App",


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_onInitialize: function _onInitialize ()
			{
				this._super ();

				this.$el.css ("opacity", 0.0);
				this.$el.html (_.template (p_html));
				// this._html = $("html");
				// this._html.css ({"font-size": "100%" });

				this.options.config = this.options.config || {};
				this.options.config.version = this.version;
				this.options.config.device = {};

				// this._initLogging ();

				_.bindAll (this, "_jQueryReadyEventHandler");
				$(document).ready (this._jQueryReadyEventHandler);

				// _.bindAll (this, "_jQueryWindowLoadEventHandler");
				// $(window).load (this._jQueryWindowLoadEventHandler);
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_jQueryReadyEventHandler: function _jQueryReadyEventHandler ()
			{
				LogCenter.log (this, "");
				// window.alert ("_jQueryReadyEventHandler");

				this._checkBrowser ();
				this._initView ();
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_checkBrowser: function _checkBrowser ()
			{
				_.extend (this.options.config.device, new UAParser ().getResult ());

				return true;
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_initView: function _initView ()
			{
				// TweenLite.defaultOverwrite = "all";
				TweenLite.defaultEase = Cubic.easeInOut;

				this._initResizeEvent ();

				// new PostTest ({"config": this.options.config });
				new UploadView ({"config": this.options.config }).$el.appendTo (this.$el);
				// new HeaderController ({"config": this.options.config });

				_.bindAll (this, "_onClick");

				this._btnHowTo = this.$el.find ("#_howTo");
				this._btnHowTo.on ("click", this._onClick);

				this._btnVideoUpload = this.$el.find ("#_videoUpload");
				this._btnVideoUpload.on ("click", this._onClick);

				this._btnNewsletter = this.$el.find ("#_newsletter");
				this._btnNewsletter.on ("click", this._onClick);

				this._renderSocials ();

				TweenLite.to (this.$el, 0.3, {"delay": 0.0, "autoAlpha": 1 });

				// this._eventBus.trigger ("OPEN_FORM", {"currentTarget": {"id": "_videoUpload"}});
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_renderSocials: function _renderSocials (p_event)
			{
				var shList = this.$el.find (".shList");
				var domain = "https://heldenchor.universalchannel.de/";

				var sh = "<li class='txt'>Teilen</li>";
				shList.append (sh);

				var fb = "<li class='fb'><a target='_blank' href='https://www.facebook.com/share.php?u=https://heldenchor.universalchannel.de/'></a></li>";
				shList.append (fb);

				var twitter = "<li class='tw'><a target='_blank' href='http://twitter.com/intent/tweet?url=" + domain + "'></a></li>";
				shList.append (twitter);

				// var gp = "<li class='sh gp'><a target='_blank' href='https://plus.google.com/share?url=" + domain + "'></a></li>";
				// shList.append (gp);

				var whatsApp = "<li class='wa'><a target='_blank' href='whatsapp://send?text=" + domain + "'></a></li>";
				shList.append (whatsApp);
			},


			_onClick: function _onClick (p_event)
			{
				this._eventBus.trigger ("OPEN_FORM", p_event);
			},


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


			_initResizeEvent: function _initResizeEvent ()
			{
				// this._html = $(":root");
				this._html = $("html");

				// this.addResizeEvent ();
				this.addResizeEvent (false);

				this.resize ();
				// TweenLite.delayedCall (0.5, this.resize);
				// this.removeResizeEvent ();
			},



			resize: function resize ()
			{
				_.extend (this.options.config.device, SNDLib.Utils.getDeviceSpecs (this._window.width (), this._window.height ()));

				// var clientWidth = this.$el.width ();
				var clientWidth = this._html.width ();
				// LogCenter.log (this, "clientWidth: " + clientWidth);

				var desktopBreakPoint = 1800;
				var desktopWidth = 1280;

				var tabletBreakPoint = 1024;
				// var tabletWidth = 1024;
				var tabletWidth = 1280;

				var phoneBreakPoint = 520;
				var phoneWidth = 320;

				var scale = 1;


				if (clientWidth > desktopBreakPoint)
				{
					scale = desktopBreakPoint / desktopWidth;
					this.$el.removeClass ("mobile");
					this.$el.removeClass ("phone");
					this.$el.removeClass ("tablet");
					this.$el.addClass ("desktop");
				}
				else if (clientWidth <= desktopBreakPoint && clientWidth > tabletBreakPoint)
				{
					scale = clientWidth / desktopWidth;
					this.$el.removeClass ("mobile");
					this.$el.removeClass ("phone");
					this.$el.removeClass ("tablet");
					this.$el.addClass ("desktop");
				}
				else if (clientWidth <= tabletBreakPoint && clientWidth > phoneBreakPoint)
				{
					scale = clientWidth / tabletWidth;
					this.$el.addClass ("mobile");
					this.$el.removeClass ("phone");
					this.$el.addClass ("tablet");
					this.$el.removeClass ("desktop");
				}
				else if (clientWidth <= phoneBreakPoint)
				{
					scale = clientWidth / phoneWidth;
					this.$el.addClass ("mobile");
					this.$el.addClass ("phone");
					this.$el.removeClass ("tablet");
					this.$el.removeClass ("desktop");
				}

				// LogCenter.log (this, "scale: " + scale);
				this._html.css ({"font-size": (scale * 100) + "%" });

				// LogCenter.log (this, "");
				// LogCenter.logObjectProps (this, this.options.config, "config");
				// LogCenter.logObjectProps (this, device, "device");

				// LogCenter.log (this, "versionLoad: " + this.options.config.versionLoad);
				// LogCenter.log (this, "version: " + this.options.config.version);
				// LogCenter.log (this, "os.name: " + this.options.config.device.os.name);
			}


			//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		});


		return App;
	}
);
