<?php


//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: WP REST API


function update_post_hook ()
{
	$restservice = new Restservice_Controller ();
	return $restservice->update_post ();
}


add_action ('rest_api_init', function ()
{
	register_rest_route ('restservice', '/update_post', [
		'methods' => 'POST',
		'callback' => 'update_post_hook'
	] );
} );


class Restservice_Controller
{


	public function update_post ()
	{
		$postId = $_POST['postId'];
		$field_key = $_POST['field_key'];
		$formType = $_POST['formType'];
		$forename = $_POST['forename'];
		$surname = $_POST['surname'];
		$email = $_POST['email'];
		$feuerwehr = $_POST['feuerwehr'];
		$place = $_POST['place'];
		// PC::debug ("$formType: ".$formType);


		if($formType === "_videoUpload")
		{
			$file = $_FILES['file'];
			PC::debug ($file);

			$fields = get_field ($field_key, $postId);
			if (! $fields) $fields = array ();

			// if ( !function_exists(‘wp_handle_upload’) ) {
			// require_once(ABSPATH . ‘wp-admin/includes/file.php’);
			// }
			// $movefile = wp_handle_upload ($file, array( 'test_form' => false ));


			// http://php.net/manual/de/function.date.php
			date_default_timezone_set ("Europe/Berlin");
			// $date = date ("d.m.Y, H:i");
			$date = date ("ymd_Hi");


			$filename = $date."_".$forename."_".$surname."_".uniqid().".mp4";
			// $filename = $forename."_".$surname."_".$date.".mp4";
			$path = "wp-content/uploads/videos/".$filename;
			$content = file_get_contents ($file["tmp_name"]);
			file_put_contents ($path, $content);

			array_push ($fields, array("forename" => $forename, "surname" => $surname, "email" => $email, "filename" => $filename, "feuerwehr" => $feuerwehr, "place" => $place ));
			update_field ($field_key, $fields, $postId);


			// https://css-tricks.com/sending-nice-html-email-with-php/
			$to = $email;
			$subject = 'Universal Channel Heldenchor | Video Upload';
			$header = 'From: automail@universalchannel.de' . "\r\n";
			// $header .= 'Bcc: p.neumann@supernice-dev.com' . "\r\n";
			$header .= 'Bcc: Lisa.Uttinger@heye.de' . "\r\n";
			$header .= "MIME-Version: 1.0\r\n";
			$header .= "Content-Type: text/html; charset=utf-8\r\n";
			$message =  '<p>Hallo '.$forename.'!</p>';
			$message .= '<p>Vielen Dank, dass ihr euer Video hochgeladen habt! Du bist mit deinen Kollegen jetzt Teil des Heldenchors.</p>';
			$message .= '<p>Sobald das Musikvideo fertig ist, bekommt ihr Nachricht von uns.</p>';
			$message .= '<p></p>';
			$message .= '<p>Bis bald!</p>';
			$message .= '<p>Viele Grüße, Universal Channel Deutschland</p>';
			mail($to, $subject, $message, $header);


			// if(isset($_POST['checkBoxNewsletter']) && $_POST['checkBoxNewsletter'] == 'on')
			// {
			// 	$checkBoxNewsletter = $_POST['checkBoxNewsletter'];
			// 	PC::debug ("checkBoxNewsletter: ".$checkBoxNewsletter);
			// }

			return $fields;
		}
		else
		{
			$to = $email;
			$subject = 'Universal Channel Heldenchor | Anleitung&Song';
			$header = 'From: automail@universalchannel.de' . "\r\n";
			// $header .= 'Bcc: p.neumann@supernice-dev.com' . "\r\n";
			$header .= 'Bcc: Lisa.Uttinger@heye.de' . "\r\n";
			$header .= "MIME-Version: 1.0\r\n";
			$header .= "Content-Type: text/html; charset=utf-8\r\n";
			$message =  '<p>Hallo!</p>';
			$message .= '<p>Vielen Dank, dass du und deine Kollegen beim Heldenchor dabei sein wollt!</p>';
			$message .= '<p>Songtext und Video-Anleitung findet ihr hier: <a href="http://heldenchor.universalchannel.de/video_upload_anleitung.pdf">Anleitung</a></p>';
			$message .= '<p>Den Song könnt ihr hier herunter laden: <a href="http://heldenchor.universalchannel.de/song.mp3">Song</a></p>';
			$message .= '<p>Euer fertiges Video könnt ihr bis zum 03. April auf <a href="http://heldenchor.universalchannel.de">heldenchor.universalchannel.de</a> hochladen.</p>';
			$message .= '<p></p>';
			$message .= '<p>Wir freuen uns auf euren Beitrag!</p>';
			$message .= '<p>Viele Grüße, Universal Channel Deutschland</p>';
			mail($to, $subject, $message, $header);

			// if(isset($_POST['checkBoxNewsletter']) && $_POST['checkBoxNewsletter'] == 'on')
			// {
				// $checkBoxNewsletter = $_POST['checkBoxNewsletter'];
				// PC::debug ("checkBoxNewsletter: ".$checkBoxNewsletter);
			// }
		}
	}


	//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
}


?>
