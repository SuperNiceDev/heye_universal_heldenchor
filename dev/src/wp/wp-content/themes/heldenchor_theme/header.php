<!doctype html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">


	<!-- Fullscreen iOS Safari -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black"><!-- default / black / black-translucent -->

	<!-- Fullscreen iOS Chrome -->
	<meta name="mobile-web-app-capable" content="yes">

	<meta name="msapplication-tap-highlight" content="no" />


	<?php
		// $language  = pll_current_language ();
		// PC::debug ($language);

		$title = get_bloginfo ();
		$titleComplete = "";
		$description = get_bloginfo ("description");
		$keywords = "";
		$sharingImg = "";

		// global $post;
		// PC::debug ("post->ID: ".$post->ID);

		$postID = get_the_ID ();
		PC::debug ("postID: ".$postID);

		if ($postID)
		{
			// $post = get_post ($postID);
			// PC::debug ($post);

			$acf = get_fields ($postID);
			// PC::debug ("acf: ".$acf);
			PC::debug ($acf);

			// $titleComplete = $title." | ".$acf["header_title"];
			$titleComplete = $title." ".$acf["header_title"];
			$description = $acf["header_description"];
			$keywords = $acf["header_keywords"];
			$sharingImg = $acf["header_sharing_img"];
		}
		else
		{
			$titleComplete = $title;
		}

		$titleComplete = htmlspecialchars_decode ($titleComplete);

		PC::debug ("titleComplete: ".$titleComplete);
		PC::debug ("description: ".$description);
		PC::debug ("keywords: ".$keywords);
		PC::debug ("sharingImg: ".$sharingImg);
	?>

	<title><?php echo $titleComplete; ?></title>
	<meta name="description" content="<?php echo $description; ?>">
	<meta name="keywords" content="<?php echo $keywords; ?>">

	<meta property="og:url" content="https://heldenchor.universalchannel.de/">
	<meta property="og:title" content="<?php echo $titleComplete; ?>">
	<meta property="og:description" content="<?php echo $description; ?>">
	<meta property="og:image" content="<?php echo $sharingImg; ?>">


	<!-- https://dev.twitter.com/cards/types -->
	<!-- <meta name="twitter:card" content="summary"> -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="<?php echo $titleComplete; ?>">
	<meta name="twitter:title" content="<?php echo $titleComplete; ?>">
	<meta name="twitter:description" content="<?php echo $description; ?>">
	<meta name="twitter:image" content="<?php echo $sharingImg; ?>">


	<?php  //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::  ?>


	<?php
		$tplUri = get_template_directory_uri ();
		// PC::debug ($tplUri);
	?>

	<base href="<?php echo $tplUri, '/'; ?>"/>

</head>


<body>
