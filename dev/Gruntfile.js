/*global module:false*/
"use strict";
module.exports = function (grunt)
{

	grunt.initConfig ({

		// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		pkg: grunt.file.readJSON ("package.json"),
		// properties: grunt.file.readJSON ("grunt-properties/property.json"),
		// userProperties: grunt.file.readJSON ("grunt-properties/" + process.env.USERNAME + ".json"),

		// webAppDir: "src/wp/wp-content/themes/snd_wp_template_theme/webApp/",
		webAppDir: "src/wp/wp-content/themes/heldenchor_theme/webApp/",

		// destination: process.env.DEST,

		// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		// https://github.com/requirejs/r.js/blob/master/build/example.build.js

		requirejs:
		{
			deploy:
			{
				options:
				{
					// baseUrl: "XXX",
					mainConfigFile: "<%= webAppDir %>js/webApp.js",

					/*:::::::::::::::::::::::::::::::: optimze  to single file*/

					name: "webApp",
					out: "<%= webAppDir %>js_bin/webApp.js",

					/*:::::::::::::::::::::::::::::::: optimze  to modules*/

					// modules:
					// [
					// 	{
					// 		name: "webApp"
					// 	}
					// ],
					// dir: "<%= webAppDir %>js_bin",

					/*::::::::::::::::::::::::::::::::*/

					// buildCSS: true,
					// separateCSS: true,
					// optimizeCss: "standard.keepLines.keepWhitespace",

					/*::::::::::::::::::::::::::::::::*/

					buildCSS: true,
					// separateCSS: true,

					// optimize: "none",
					optimize: "uglify",
					// optimize: "uglify2",

					generateSourceMaps: true,
					wrapShim: true,
					removeCombined: true,
					findNestedDependencies: true,
					preserveLicenseComments: false
				}
			}
		}
	});



	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  load tasks

	grunt.loadNpmTasks ("grunt-contrib-requirejs");
	// grunt.loadNpmTasks ("grunt-contrib-uglify");


	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  register tasks

	grunt.registerTask ("default", ["deploy"]);

	grunt.registerTask ("deploy", ["requirejs:deploy"]);


	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
};
